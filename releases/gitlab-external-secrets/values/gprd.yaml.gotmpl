---
{{- $env := .Environment.Values | get "env_prefix" .Environment.Name }}

externalSecrets:
  diagnostics-credentials-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/diagnostic-credentials
          property: credentials
          version: "2"
        secretKey: credentials

  embedding-postgresql-gprd-password-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/embedding/postgresql
          property: password
          version: "2"
        secretKey: password

  gitlab-github-oauth2-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "github", "app_id": {{ `{{ "{{" }}` }} .app_id | quote }}, "app_secret": {{ `{{ "{{" }}` }} .app_secret | quote }}, "url": "https://github.com", "args": {"scope": "user:email"}}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/github
          property: app_id
          version: "1"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/github
          property: app_secret
          version: "1"
        secretKey: app_secret

  gitlab-github-oauth2-v3:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "github", "app_id": {{ `{{ "{{" }}` }} .app_id | quote }}, "app_secret": {{ `{{ "{{" }}` }} .app_secret | quote }}, "url": "https://github.com", "args": {"scope": "user:email"}}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/github
          property: app_id
          version: "2"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/github
          property: app_secret
          version: "2"
        secretKey: app_secret

  gitlab-bitbucket-oauth2-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "bitbucket", "app_id": {{ `{{ "{{" }}` }} .app_id | quote }}, "app_secret": {{ `{{ "{{" }}` }} .app_secret | quote }}, "url": "https://bitbucket.org/"}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/bitbucket
          property: app_id
          version: "1"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/bitbucket
          property: app_secret
          version: "1"
        secretKey: app_secret

  gitlab-bitbucket-oauth2-v3:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "bitbucket", "app_id": {{ `{{ "{{" }}` }} .app_id | quote }}, "app_secret": {{ `{{ "{{" }}` }} .app_secret | quote }}, "url": "https://bitbucket.org/"}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/bitbucket
          property: app_id
          version: "2"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/bitbucket
          property: app_secret
          version: "2"
        secretKey: app_secret

  gitlab-group-saml-oauth2-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "group_saml", "allowed_clock_drift": "2"}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/group-saml
          property: app_id
          version: "1"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/group-saml
          property: app_secret
          version: "1"
        secretKey: app_secret

  gitlab-salesforce-oauth2-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "salesforce", "app_id": {{ `{{ "{{" }}` }} .app_id | quote }}, "app_secret": {{ `{{ "{{" }}` }} .app_secret | quote }}}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/salesforce
          property: app_id
          version: "1"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/salesforce
          property: app_secret
          version: "1"
        secretKey: app_secret

  gitlab-salesforce-oauth2-v3:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          provider: >
            {"name": "salesforce", "app_id": {{ `{{ "{{" }}` }} .app_id | quote }}, "app_secret": {{ `{{ "{{" }}` }} .app_secret | quote }}}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/salesforce
          property: app_id
          version: "2"
        secretKey: app_id
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/oauth2/salesforce
          property: app_secret
          version: "2"
        secretKey: app_secret

  kas-otel-credentials-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/kas/otel
          property: otel_api_token
          version: "1"
        secretKey: otel_api_token

  kas-otel-credentials-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/kas/otel
          property: otel_api_token
          version: "2"
        secretKey: otel_api_token

  artifacts-cdn-private-key-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          cdn: |
            provider: Google
            url: https://{{ .Environment.Values | get "artifacts_cdn_domain" nil }}
            key_name: {{ $env }}-artifacts-cdn
            key: {{ `{{ "{{" }}` }} .private_key | quote }}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/artifacts-cdn
          property: private_key
          version: "1"
        secretKey: private_key

  gitlab-rack-attack-ip-whitelist-v9:
    refreshInterval: 0
    secretStoreName: gitlab-shared-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/gitlab/rack-attack
          property: ip_whitelist
          version: "9"
        secretKey: ip_whitelist

  gitlab-throttle-user-allowlist-v2:
    refreshInterval: 0
    secretStoreName: gitlab-shared-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/frontend/user-ratelimit
          property: bypasses
          version: "1"
        secretKey: allowlist

  gitlab-qa-user-agent-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/gitlab-qa
          property: user_agent
          version: "2"
        secretKey: user-agent

  gitlab-pages-tls-certificate-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/pages/tls
          property: certificate
          version: "2"
        secretKey: tls.crt
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/pages/tls
          property: private_key
          version: "2"
        secretKey: tls.key

  gitlab-zoekt-basicauth-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/zoekt/basicauth
          property: username
          version: "1"
        secretKey: gitlab_username
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/zoekt/basicauth
          property: password
          version: "1"
        secretKey: gitlab_password

  gitlab-object-storage-v3:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          gitlab-object-storage.yml: |
            provider: Google
            google_project: {{ .Environment.Values.google_project }}
            google_json_key_string: |
              {{ `{{ "{{-" }}` }} .credentials | nindent 2 }}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/google
          property: credentials
          version: "2"
        secretKey: credentials

  registry-storage-v7:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          config: |
            gcs:
              bucket: {{ .Environment.Values | get "registry_bucket_name" (printf "gitlab-%s-registry" $env) }}
              keyfile: /etc/docker/registry/storage/gcs.json
              rootdirectory: gitlab
            delete:
              enabled: true
            redirect:
              expirydelay: 15m
          gcs.json: |
            {{ `{{ "{{-" }}` }} .service_account_key -}}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/registry/google
          property: service_account_key
          version: "2"
        secretKey: service_account_key

  gitlab-clickhouse-main-password-v3:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/clickhouse
          property: password
          version: "3"
        secretKey: password
