---
{{- $env := .Environment.Values | get "env_prefix" .Environment.Name }}

externalSecrets:
  cloudflare-api-token-v1:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/cloudflare-api
          property: token
          version: "1"
        secretKey: token

  artifacts-cdn-private-key-v5:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          cdn: |
            provider: Google
            url: https://{{ .Environment.Values | get "artifacts_cdn_domain" nil }}
            key_name: {{ $env }}-artifacts-cdn
            key: {{ `{{ "{{" }}` }} .private_key | quote }}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/artifacts-cdn
          property: private_key
          version: "2"
        secretKey: private_key

  gitlab-runner-registration-credential-v2:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          runner-registration-token: {{ `{{ "{{ .runner_registration_token }}" }}` | quote }}
          runner-token: ""
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/runner/registration
          property: token
          version: "1"
        secretKey: runner_registration_token

  gitlab-pages-tls-certificate-v3a:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/pages/tls
          property: certificate
          version: "3"
        secretKey: tls.crt
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/pages/tls
          property: private_key
          version: "3"
        secretKey: tls.key

  # WIP https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/2769
  gitlab-object-storage-v3:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Merge
      template:
        data:
          gitlab-object-storage.yml: |
            provider: Google
            google_project: {{ .Environment.Values.google_project }}
            google_application_default: true

  gitlab-object-storage-v4:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          gitlab-object-storage.yml: |
            provider: Google
            google_project: {{ .Environment.Values.google_project }}
            google_json_key_string: |
              {{ `{{ "{{-" }}` }} .credentials | nindent 2 }}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/google
          property: credentials
          version: "2"
        secretKey: credentials

  # WIP https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/2769
  registry-storage-v7:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Merge
      template:
        data:
          config: |
            gcs:
              bucket: {{ .Environment.Values | get "registry_bucket_name" (printf "gitlab-%s-registry" $env) }}
              rootdirectory: gitlab
            delete:
              enabled: true
            redirect:
              expirydelay: 15m

  registry-storage-v8:
    refreshInterval: 0
    secretStoreName: gitlab-secrets
    target:
      creationPolicy: Owner
      deletionPolicy: Delete
      template:
        data:
          config: |
            gcs:
              bucket: {{ .Environment.Values | get "registry_bucket_name" (printf "gitlab-%s-registry" $env) }}
              keyfile: /etc/docker/registry/storage/gcs.json
              rootdirectory: gitlab
            delete:
              enabled: true
            redirect:
              expirydelay: 15m
          gcs.json: |
            {{ `{{ "{{-" }}` }} .service_account_key -}}
    data:
      - remoteRef:
          key: env/{{ $env }}/ns/gitlab/registry/google
          property: service_account_key
          version: "2"
        secretKey: service_account_key
