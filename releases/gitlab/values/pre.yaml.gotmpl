---

gitlab-runner:
  gitlabUrl: {{ .Values.gitlab_endpoint }}
  install: true
  rbac:
    create: true
  runners:
    config: |
      [[runners]]
        [runners.kubernetes]
          namespace = "gitlab"
          image = "debian:buster-slim"
          privileged = true
          allow_privilege_escalation = true
        [[runners.kubernetes.volumes.empty_dir]]
          name = "docker-certs"
          mount_path = "/certs/client"
          medium = "Memory"

# Force PreProd config for https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1163
nginx-ingress:
  controller:
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                  - key: app.kubernetes.io/name
                    operator: In
                    values:
                      - ingress-nginx
                  - key: app.kubernetes.io/instance
                    operator: In
                    values:
                      - ingress-nginx
                  - key: app.kubernetes.io/component
                    operator: In
                    values:
                      - controller
              topologyKey: kubernetes.io/hostname

registry:
  middleware:
    storage:
      - name: googlecdn
        options:
          baseurl: cdn.registry.pre.gitlab-static.net
          privatekeySecret:
            secret: registry-cdn-private-key-v2
            key: private-key
          keyname: pre-registry-cdn
          ipfilteredby: gcp
  database:
    enabled: true
    host: 10.33.1.2
    user: registry
    name: registry_production
    password:
      secret: registry-postgresql-password-v2
    pool:
      maxopen: 5
      maxidle: 5
      maxlifetime: 5m
  extraEnv:
    SKIP_POST_DEPLOYMENT_MIGRATIONS: true
    REGISTRY_FF_ACCURATE_LAYER_MEDIA_TYPES: true
    # https://gitlab.com/gitlab-org/container-registry/-/issues/1036
    REGISTRY_FF_ONGOING_RENAME_CHECK: true
  gc:
    disabled: false
    maxbackoff: 30m
    reviewafter: 5m
    noidlebackoff: true
    blobs:
      storagetimeout: 2s
  service:
    # gcloud compute address registry-gke-pre
    loadBalancerIP: 10.232.20.8
  maintenance:
    uploadpurging:
      enabled: false
  validation:
    disabled: false
    manifests:
      referencelimit: 200
      payloadsizelimit: 256000
      urls:
        # this is needed to keep backwards compatibility when `validation.disabled: false`, as by default an empty `allow` means no URLs are allowed
        allow:
          - .*
  redis:
    cache:
      enabled: true
      host: mymaster
      sentinels:
        - host: redis-registry-cache-node-0.redis-registry-cache.pre.gke.gitlab.net
          port: 26379
        - host: redis-registry-cache-node-1.redis-registry-cache.pre.gke.gitlab.net
          port: 26379
        - host: redis-registry-cache-node-2.redis-registry-cache.pre.gke.gitlab.net
          port: 26379
      password:
        enabled: true
        secret: redis-registry-cache-password-v1
        key: redis-password
      dialtimeout: 2s
      readtimeout: 2s
      writetimeout: 2s
      pool:
        size: 10
        maxlifetime: 1h
        idletimeout: 5m
  log:
    level: debug
  storage:
    secret: registry-storage-v8

  networkpolicy:
    egress:
      # The following rules enable traffic to all external
      # endpoints, except the metadata service and the local
      # network (except DNS and DB requests)
      rules:
        - to:
            - ipBlock:
                cidr: 10.0.0.0/8
          ports:
            # cloudsql in pre
            - port: 5432
              protocol: TCP
            # redis
            - port: 6379
              protocol: TCP
            # pgbouncer in gstg and gprd
            - port: 6432
              protocol: TCP
            # sentinel
            - port: 26379
              protocol: TCP

        - ports:
            - port: 53
              protocol: UDP
            - port: 53
              protocol: TCP

        # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
        - to:
            - ipBlock:
                cidr: 169.254.169.252/32
          ports:
            - port: 988
              protocol: TCP
        - to:
            - ipBlock:
                cidr: 169.254.169.254/32
          ports:
            - port: 80
              protocol: TCP

        - to:
            - ipBlock:
                cidr: 0.0.0.0/0
                except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32

gitlab:
  gitlab-pages:
    hpa:
      minReplicas: 1
      cpu:
        targetAverageValue: 400m
    resources:
      requests:
        cpu: 500m
        memory: 70M
      limits:
        memory: 1G
    extraEnv:
      FF_HANDLE_CACHE_HEADERS: "true"
      FF_ENFORCE_IP_RATE_LIMITS: "true"
      FF_ENFORCE_DOMAIN_RATE_LIMITS: "true"
      FF_CONFIGURABLE_ROOT_DIR: "true"
    rateLimitSourceIP: 20
    rateLimitSourceIPBurst: 300
    rateLimitDomain: 100
    rateLimitDomainBurst: 500
    rateLimitTLSDomain: 30
    rateLimitTLSDomainBurst: 100
    zipCache:
      expiration: 300s
    networkpolicy:
      egress:
        rules:
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              - port: 8181
                protocol: TCP

          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32
  webservice:
    hpa:
      minReplicas: 2
      maxReplicas: 5
    # blackoutSeconds, minReplicas, initialDelaySeconds, terminationGracePeriodSeconds
    # set temporarily for
    # testing https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1509
    shutdown:
      blackoutSeconds: 0
    deployments:
      api:
        extraEnv:
          GITLAB_CONTINUOUS_PROFILING: stackdriver?service=workhorse-api
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"api\", \"stage\": \"main\"}"
        ingress:
          path: '/api'
        service:
          # gcloud compute address api-gke-pre
          loadBalancerIP: 10.232.20.11
      git:
        service:
          # gcloud compute address git-https-gke-pre
          loadBalancerIP: 10.232.20.5
        shutdown:
          blackoutSeconds: 0
      web:
        service:
          # gcloud compute address web-gke-pre
          loadBalancerIP: 10.232.20.3
      websockets:
        deployment:
          terminationGracePeriodSeconds: 30
        service:
          # gcloud compute address websockets-gke-pre
          loadBalancerIP: 10.232.20.6
      internal-api:
        deployment:
          terminationGracePeriodSeconds: 65
        extraEnv:
          GITLAB_SENTRY_EXTRA_TAGS: "{\"type\": \"internal-api\", \"stage\": \"main\"}"
        shutdown:
          blackoutSeconds: 10
        hpa:
          minReplicas: 2
          maxReplicas: 5
        resources:
          requests:
            cpu: 500m
            memory: 1.25G
        workhorse:
          extraArgs: '-apiLimit 9 -apiQueueDuration 30s -apiQueueLimit 2000 -apiCiLongPollingDuration 50s -propagateCorrelationID'
        workerProcesses: 6
    rack_attack:
      git_basic_auth:
        enabled: false
    extraEnv:
      ENABLE_RBTRACE: 1
    workerProcesses: 2
    workhorse:
      resources:
        limits:
          memory: 1G
        requests:
          cpu: 100m
          memory: 50M
    resources:
      limits:
        memory: 4.0G
      requests:
        cpu: 1
        memory: 1.25G
    # TODO: remove after testing, move to `values.yaml.gotmpl`
    networkpolicy:
      egress:
        # The following rules enable traffic to all external
        # endpoints, except the metadata service and the local
        # network (except DNS, gitaly, redis and postgres)
        rules:
          # Allow all traffic except internal network and metadata service
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32

          # Allow traffic for DNS + consul DNS
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP
              - port: 8600
                protocol: TCP
              - port: 8600
                protocol: UDP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow internal traffic to Container Registry
          - to:
              - podSelector:
                  matchLabels:
                    app: registry
            ports:
              - port: 5000
                protocol: TCP

          # Allow internal traffic from API nodes to Kas
          - to:
              - podSelector:
                  matchLabels:
                    app: kas
            ports:
              - port: 8153
                protocol: TCP

          # Allow traffic to Redis
          - ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP
              # gstg, gprd Redis
              - port: 26379
                protocol: TCP

          # Allow traffic to Postgresql
          - ports:
              # pre CloudSQL
              - port: 5432
                protocol: TCP
              # gstg, gprd pgbouncer
              - port: 6432
                protocol: TCP
              - port: 6433
                protocol: TCP
              - port: 6434
                protocol: TCP
              - port: 6435
                protocol: TCP
              - port: 6436
                protocol: TCP
              - port: 6437
                protocol: TCP
              - port: 6438
                protocol: TCP
              - port: 6439
                protocol: TCP
              - port: 6440
                protocol: TCP
              - port: 6441
                protocol: TCP
              - port: 6442
                protocol: TCP
              - port: 6443
                protocol: TCP

          # Allow traffic to Gitaly
          - ports:
              # Gitaly non-TLS
              - port: 9999
                protocol: TCP
              # Gitaly TLS
              - port: 9998
                protocol: TCP
              # Praefect
              - port: 2305
                protocol: TCP

          # Allow internal traffic to thanos
          - ports:
              - port: 9090
                protocol: TCP

  gitlab-shell:
    workhorse:
      serviceName: webservice-internal-api
    extraEnv:
      GITLAB_CONTINUOUS_PROFILING: stackdriver?service=gitlab-shell
    metrics:
      enabled: true
    minReplicas: 2
    maxReplicas: 5
    service:
      # gcloud compute address ssh-gke-pre
      loadBalancerIP: 10.232.20.9
    serviceAccount:
      annotations:
        iam.gke.io/gcp-service-account: gitlab-gitlab-shell@{{ .Values.google_project }}.iam.gserviceaccount.com
    sshDaemon: gitlab-sshd

  mailroom:
    workhorse:
      serviceName: webservice-api
    networkpolicy:
      egress:
        rules:
          # Allow DNS
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow IMAP
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                    - 10.0.0.0/8
                    - 169.254.169.252/32
                    - 169.254.169.254/32
            ports:
              - port: 993
                protocol: TCP

          # Allow Redis
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP

              # gstg, gprd
              - port: 26379
                protocol: TCP

          # Allow talking to our internal API endpoints
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              - port: 443
                protocol: TCP
              - port: 11443
                protocol: TCP
              - port: 8181
                protocol: TCP

  sidekiq:
    pods:
      - name: catchall
        common:
          labels:
            shard: catchall
        concurrency: 25
        keda:
          minReplicaCount: 1
          maxReplicaCount: 5
          pollingInterval: 15
          behavior:
            scaleDown:
              stabilizationWindowSeconds: 300
              policies:
                - type: Percent
                  value: 10
                  periodSeconds: 15
            scaleUp:
              policies:
                - type: Percent
                  value: 100
                  periodSeconds: 15
          triggers:
            - type: cpu
              metricType: AverageValue
              metadata:
                value: 450m
            - type: prometheus
              metricType: Value
              metadata:
                serverAddress: http://gitlab-monitoring-promethe-prometheus.monitoring.svc.cluster.local:9090
                query: sum(avg_over_time(sidekiq_running_jobs{shard="catchall"}[15s])) / sum(avg_over_time(sidekiq_concurrency{shard="catchall"}[15s]))
                threshold: '0.75'
                ignoreNullValues: 'false'
            - type: cron
              metadata:
                timezone: UTC
                start: 55 * * * *
                end: 10 * * * *
                desiredReplicas: "3"
    extraEnv:
      GITLAB_MEMORY_WATCHDOG_ENABLED: "true"
    psql:
      host: 10.33.0.48
    networkpolicy:
      egress:
        # The following rules enable traffic to all external
        # endpoints, except the metadata service and the local
        # network (except DNS, gitaly, redis and postgres)
        rules:
          # Allow all traffic except internal network and metadata service
          - to:
              - ipBlock:
                  cidr: 0.0.0.0/0
                  except:
                  - 10.0.0.0/8
                  - 169.254.169.252/32
                  - 169.254.169.254/32

          # Allow DNS traffic to the metadata server
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP

          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP

          # Allow internal traffic to consul for consul DNS
          - to:
              - namespaceSelector: {}
                podSelector:
                  matchLabels:
                    app: consul
            ports:
              - port: 8600
                protocol: TCP
              - port: 8600
                protocol: UDP

          # Allow internal traffic to Redis
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP
              # gstg, gprd Redis
              - port: 26379
                protocol: TCP

          # Allow internal traffic to Postgresql
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # pre CloudSQL
              - port: 5432
                protocol: TCP
              # gstg, gprd pgbouncer
              - port: 6432
                protocol: TCP
              - port: 6433
                protocol: TCP
              - port: 6434
                protocol: TCP
              - port: 6435
                protocol: TCP
              - port: 6436
                protocol: TCP
              - port: 6437
                protocol: TCP
              - port: 6438
                protocol: TCP
              - port: 6439
                protocol: TCP
              - port: 6440
                protocol: TCP
              - port: 6441
                protocol: TCP
              - port: 6442
                protocol: TCP
              - port: 6443
                protocol: TCP

          # Allow internal traffic to Gitaly
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # Gitaly non-TLS
              - port: 9999
                protocol: TCP
              # Gitaly TLS
              - port: 9998
                protocol: TCP
              # Praefect
              - port: 2305
                protocol: TCP

          # Allow internal traffic to Container Registry
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              # Container Registry
              - port: 5000
                protocol: TCP

            # Allow internal traffic to thanos
          - to:
              - ipBlock:
                  cidr: 10.0.0.0/8
            ports:
              - port: 9090
                protocol: TCP
  kas:
    image:
      tag: 16-5-202310180205-d0d3b067e2d
    minReplicas: 2
    maxReplicas: 5
    service:
      # gcloud compute address kas-internal-gke-pre
      loadBalancerIP: 10.232.20.4
    workhorse:
      host: 'gitlab-webservice-api.gitlab.svc'
    networkpolicy:
      egress:
        rules:
          # Disables all outgoing traffic except to specific ports on the internal network
          # Allow traffic for DNS
          - ports:
              - port: 53
                protocol: UDP
              - port: 53
                protocol: TCP
          # Allow talking to our internal API endpoints
          - to:
              - podSelector:
                  matchLabels:
                    gitlab.com/webservice-name: api
            ports:
              - port: 8181
                protocol: TCP
          # Allow internal traffic to Gitaly
          - ports:
              # Gitaly non-TLS
              - port: 9999
                protocol: TCP
              # Gitaly TLS
              - port: 9998
                protocol: TCP
              # Praefect
              - port: 2305
                protocol: TCP
          # Allow internal traffic to Redis
          - ports:
              # pre Memorystore instance
              - port: 6379
                protocol: TCP
          # Allow all outgoing HTTPS/443
          - ports:
              - port: 443
                protocol: TCP
          # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
          - to:
              - ipBlock:
                  cidr: 169.254.169.252/32
            ports:
              - port: 988
                protocol: TCP
          - to:
              - ipBlock:
                  cidr: 169.254.169.254/32
            ports:
              - port: 80
                protocol: TCP
          # KAS internal traffic
          - to:
              - podSelector:
                  matchLabels:
                    app: kas
            ports:
              - port: 8155
                protocol: TCP
global:
  ingress:
    tls:
      secretName: gitlab-pages-tls-certificate-v3a
  appConfig:
    artifacts:
      bucket: gitlab-pre-artifacts
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file
      cdn:
        secret: artifacts-cdn-private-key-v5
    contentSecurityPolicy:
      enabled: true
      report_only: false
      directives:
        connect_src: "'self' https://pre.gitlab.com https://pre.gitlab-static.net wss://pre.gitlab.com https://sentry.gitlab.net https://customers.gitlab.com https://snowplow.trx.gitlab.net https://sourcegraph.com https://ec2.ap-east-1.amazonaws.com https://ec2.ap-northeast-1.amazonaws.com https://ec2.ap-northeast-2.amazonaws.com https://ec2.ap-northeast-3.amazonaws.com https://ec2.ap-south-1.amazonaws.com https://ec2.ap-southeast-1.amazonaws.com https://ec2.ap-southeast-2.amazonaws.com https://ec2.ca-central-1.amazonaws.com https://ec2.eu-central-1.amazonaws.com https://ec2.eu-north-1.amazonaws.com https://ec2.eu-west-1.amazonaws.com https://ec2.eu-west-2.amazonaws.com https://ec2.eu-west-3.amazonaws.com https://ec2.me-south-1.amazonaws.com https://ec2.sa-east-1.amazonaws.com https://ec2.us-east-1.amazonaws.com https://ec2.us-east-2.amazonaws.com https://ec2.us-west-1.amazonaws.com https://ec2.us-west-2.amazonaws.com https://ec2.af-south-1.amazonaws.com https://iam.amazonaws.com"
        default_src: "'self' https://pre.gitlab-static.net"
        frame_ancestors: "'self'"
        frame_src: "'self' https://pre.gitlab-static.net https://www.google.com/recaptcha/ https://www.recaptcha.net/ https://content.googleapis.com https://content-cloudresourcemanager.googleapis.com https://content-compute.googleapis.com https://content-cloudbilling.googleapis.com https://*.codesandbox.io"
        img_src: "* data: blob:"
        object_src: "'none'"
        report_uri: "https://sentry.gitlab.net/api/22/security/?sentry_key=e9401448c5c04c39823793199b8f7c49"
        script_src: "'self' 'unsafe-inline' 'unsafe-eval' https://pre.gitlab-static.net https://www.google.com/recaptcha/ https://www.gstatic.com/recaptcha/ https://www.recaptcha.net/ https://apis.google.com"
        style_src: "'self' 'unsafe-inline' https://pre.gitlab-static.net"
        worker_src: "https://pre.gitlab-static.net https://pre.gitlab.com blob: data:"
    dependencyProxy:
      enabled: true
      bucket: gitlab-pre-dependency-proxy
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file
    externalDiffs:
      bucket: gitlab-pre-external-diffs
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file
    incomingEmail:
      address: "incoming-pre+%{key}@incoming.gitlab.com"
      user: incoming-pre@incoming.gitlab.com
      deliveryMethod: webhook
    lfs:
      bucket: gitlab-pre-lfs-objects
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file
    object_store:
      enabled: true
      connection:
        secret: gitlab-object-storage-v4
        key: gitlab-object-storage.yml
    omniauth:
      providers:
        - secret: gitlab-google-oauth2-v2
    packages:
      bucket: gitlab-pre-package-repo
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file
    sentry:
      clientside_dsn: https://f5573e26de8f4293b285e556c35dfd6e@new-sentry.gitlab.net/4
      dsn: https://dafccd342c484bf9a1d2c20f769eb9cc@new-sentry.gitlab.net/3
      environment: pre
    terraformState:
      bucket: gitlab-pre-terraform-state
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file
    uploads:
      bucket: gitlab-pre-uploads
      connection: null # we need to reset this key to an empty map to ignore the value from previous loaded yaml file

  email:
    from: notify@mg.pre.gitlab.com
    reply_to: noreply@pre.gitlab.com

  gitaly:
    external:
      - hostname: gitaly-01-sv-pre.c.gitlab-pre.internal
        name: default
        port: "9999"
        tlsEnabled: false
      - hostname: praefect-01-stor-pre.c.gitlab-pre.internal
        name: praefect
        port: "2305"
        tlsEnabled: false
      - hostname: gitaly-03-sv-pre.c.gitlab-pre.internal
        name: gitaly-03
        port: "9999"
        tlsEnabled: false

  hosts:
    # gcloud compute address nginx-gke-pre
    externalIP: 10.232.20.7
    gitlab:
      name: pre.gitlab.com
    kas:
      name: kas.pre.gitlab.com
    registry:
      name: registry.pre.gitlab.com

  pages:
    enabled: true
    externalHttp:
      # gcloud compute address pages-gke-pre
      - 10.232.20.10
    externalHttps:
      # gcloud compute address pages-gke-pre
      - 10.232.20.10
    host: pre.gitlab.io
    objectStore:
      connection:
        secret: gitlab-object-storage-v4

  psql:
    host: 10.33.0.48

  redis:
    # GCP MemoryStore
    host: 10.232.7.3
    port: "6379"
    rateLimiting:
      user: rails
      password:
        enabled: true
        secret: gitlab-redis-cluster-ratelimiting-rails-credential-v2
        key: password
      cluster:
      - host: redis-cluster-ratelimiting-shard-01-01-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-01-02-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-01-03-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-02-01-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-02-02-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-02-03-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-03-01-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-03-02-db-pre.c.gitlab-pre.internal
      - host: redis-cluster-ratelimiting-shard-03-03-db-pre.c.gitlab-pre.internal
    repositoryCache:
      host: pre-redis-repository-cache
      password:
        enabled: true
        secret: gitlab-redis-credential-v2
        key: password
      sentinels:
      - host: redis-repository-cache-01-db-pre.c.gitlab-pre.internal
        port: 26379
      - host: redis-repository-cache-02-db-pre.c.gitlab-pre.internal
        port: 26379
      - host: redis-repository-cache-03-db-pre.c.gitlab-pre.internal
        port: 26379

  runner:
    registrationToken:
      secret: gitlab-runner-registration-credential-v2

  smtp:
    domain: mg.pre.gitlab.com
    user_name: postmaster@mg.pre.gitlab.com

gitlab-zoekt:
  networkpolicy:
    egress:
      rules:
        - to:
            - ipBlock: # Allow all egress to the internet but nothing internal. Needed to clone repos from gitlab.com. We could be more specific if we have an IP range for gitlab.com public interface for cloning
                cidr: 0.0.0.0/0
                except:
                - 10.0.0.0/8
                - 192.168.0.0/16
                - 172.16.0.0/12
                - 169.254.169.252/32
                - 169.254.169.254/32
          ports:
            - protocol: TCP
              port: 443
        # Allow traffic to GCE metadata service - https://cloud.google.com/kubernetes-engine/docs/troubleshooting/troubleshooting-security#pod_cant_authenticate_to
        - to:
            - ipBlock:
                cidr: 169.254.169.252/32
          ports:
            - port: 988
              protocol: TCP
        - to:
            - ipBlock:
                cidr: 169.254.169.254/32
          ports:
            - port: 80
              protocol: TCP
        # Allow DNS
        - ports:
            - port: 53
              protocol: UDP
            - port: 53
              protocol: TCP
