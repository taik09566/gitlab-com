## Gitlab Extras

This folder contains the [`raw` chart](./charts/raw/), which allows us to template and deploy `helmfile` resources using the [`values.yaml.gotmpl`](./values.yaml.gotmpl) file.

### Using the templates

In order to actually use the resource templates defined in [`values.yaml.gotmpl`](./values.yaml.gotmpl), `helmfiles` values need to be set in `<environment>.yaml` files under the [`bases/` folder](../../bases/). `.Values | getOrNil` are referring to `helmfiles` values, thus the [values under `releases`](../gitlab-external-secrets/values/) will not affect it.

### Flagger Canary
One of the resources templated in the [`values.yaml.gotmpl`](./values.yaml.gotmpl) file is a `Canary` Flagger custom resource. Its values in the template is passed in by defining values under `.Values.gitlab_extras.flagger_canaries`. For more details, refer to the documentation at [`FLAGGER-CANARY.md`](./FLAGGER-CANARY.md).
