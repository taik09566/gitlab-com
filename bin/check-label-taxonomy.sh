#!/bin/bash

set -euo pipefail

declare -a allowlist_regex=(
  # Charts
  ".*gitlab-runner.*"
  ".*vault-secrets.*"
  ".*gitlab-zoekt.*" # https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-zoekt/-/merge_requests/6
  # Chart-specific resources
  ".*webservice/templates/networkpolicy.yaml$"
  # Tests
  ".*test.*"
  # Resource types
  ".*serviceaccount\.yaml$"
  ".*chart-info\.yaml$"
  ".*resources\.yaml$"
)

declare -a all_flagged_manifests=()
declare -a violating_manifests=()

get_all_flagged_manifests() {
  # Look through files under manifests folder to look for type, tier, stage, shard labels for templated resources
  # If any resource misses any of those labels, add the file path to flagged_manifests array
  while IFS= read -r -d '' manifest; do
    if [[ "$(yq <"$manifest" '.metadata.labels.type, .metadata.labels.tier, .metadata.labels.stage, .metadata.labels.shard')" == *"null"* ]]; then
      all_flagged_manifests+=("${manifest}")
    fi
  done < <(find manifests -name "*.yaml" -type f -print0)
}

filter_flagged_manifests() {
  # This filters out the file paths that matches on allowlist_regex so we don't alert on that.
  for manifest in "${all_flagged_manifests[@]}"; do
    for regex in "${allowlist_regex[@]}"; do
      if [[ "${manifest}" =~ ${regex} ]]; then
        continue 2
      fi
    done
    violating_manifests+=("${manifest}")
  done
}

usage() {
  cat <<EOF
USAGE:
$0

$0: This tool is used in CI jobs for checking manifest resources that are missing labels necessary for metrics to function properly.
It checks for the following labels: type, tier, stage, shard.
If any of those labels are missing from resources in the manifest file, and the manifest is not part of the allowlist,
it outputs a list of manifest file locations and error out.
If there are no manifests under the manifests folder, try running the 'bin/k-ctl template' prior to this script.

e.g. CLUSTER=gstg-gitlab-gke REGION=us-east1 ./bin/k-ctl -e gstg -D template && ./bin/check-label-taxonomy.sh # locally
EOF
}

main() {
  if [[ $# -ne 0 ]]; then
    usage
    exit 1
  fi

  get_all_flagged_manifests
  filter_flagged_manifests

  if ((${#violating_manifests[@]})); then
    echo "There are resources without proper labels (labels.type|tier|stage|shard) and the following are the manifests that got generated without them. To allow the manifests to bypass this check, add them in the allowlist in /bin/check-label-taxonomy.sh"
    for manifest in "${violating_manifests[@]}"; do
      echo "${manifest}"
    done
    exit 1
  fi

  echo "All resources contain proper labels (labels.type|tier|stage|shard)"
}

main "$@"
